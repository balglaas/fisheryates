#include <stdio.h>
//#include <string.h>
#include "fisheryates.h"

#define N 20

struct st_work
{
  int id;
  char data[32];
}

main(int argc,char *argv[])
{
  int i;
  struct st_work work[N];
  
  for (i=0; i<N; ++i)
  {
    work[i].id=i;
    sprintf(work[i].data,"Data%03d",i);
  }
  fisher_yates(work,N,sizeof(struct st_work));
  for (i=0; i<N; ++i)
  {
    printf("%2d %s\n",work[i].id,work[i].data);
  }
}
