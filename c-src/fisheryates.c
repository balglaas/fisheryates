#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include "fisheryates.h"

int fisher_yates(void *data,size_t number,size_t size)
{
  int i,r;
  void *buffer;

  srand(time(NULL));
  buffer=malloc(size);
  if (buffer==NULL)
  {
    return(errno);
  }
  for (i=(number-1); i>0; --i)
  {
    r=rand()%(i+1);
    if (r!=i)
    {
      memcpy(buffer,&data[i*size],size);
      memcpy(&data[i*size],&data[r*size],size);
      memcpy(&data[r*size],buffer,size);
    }
  }
  free(buffer);
  return(0);
}
