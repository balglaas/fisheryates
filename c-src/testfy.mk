CFLAGS=-c -g 
LFLAGS=

testfy:	testfy.o fisheryates.o
	gcc testfy.o ${LFLAGS} -o testfy fisheryates.o
testfy.o:	testfy.c
	gcc ${CFLAGS} testfy.c
fisheryates.o:	fisheryates.c
	gcc ${CFLAGS} fisheryates.c
