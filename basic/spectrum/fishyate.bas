  10 RANDOMIZE
  20 LET n=8
  30 DIM a(n)
  40 FOR i=1 TO n
  50 LET a(i)=i
  60 NEXT i
  70 FOR i=1 TO n
  80 LET top=n+1-i
  90 LET r=INT (RND*top)+1
 100 IF r=top THEN GO TO 140
 110 LET s=a(r)
 120 LET a(r)=a(top)
 130 LET a(top)=s
 140 NEXT i
 150 FOR i=1 TO n
 160 PRINT a(i);" ";
 170 NEXT i
 180 STOP
