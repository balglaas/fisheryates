function FisherYates(list)
{
  var l=list.length;
  for (var i=l-1; i>0; --i)
  {
    var r=Math.floor(Math.random()*l);
    if (r!=i)
    {
      var c=list[i];
      list[i]=list[r];
      list[r]=c;
    }
  }
}
